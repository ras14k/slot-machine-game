#!/bin/bash

cd prog1/
qmake
make

cd ../prog2/
qmake
make

cd ../
mkdir build

cp prog1/prog1 build/
cp prog2/prog2 build/
