# Slot Machine Game

Simple slot machine game (based on two applications with socket communication) with one-way chat.

### Build instructions:

apt-get update  
apt-get install git -y  
git clone https://gitlab.com/ras14k/slot-machine-game.git  

cd slot-machine-game/  
chmod a+x install_dep.sh  
chmod a+x build.sh  

./install_dep.sh  
./build.sh  


### Usage:

cd build/  
./prog1  

If both applications are in the same directory, then ./prog1 should start ./prog2. If not, run ./prog1 application first and then run ./prog2.  
In case of any problems with ./prog1 application check if there is not remaining socket file in /tmp directory, delete this file (rm /tmp/slot_machine) and restart both applications.  


### Additional info:

If you want to build and test this project on Docker, just install docker-ce from https://docs.docker.com/install/linux/docker-ce/ubuntu/ and run the following command:  
sudo docker run -it --net=host --device /dev/dri --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" ubuntu:16.04

Gitlab Runner script (.gitlab-ci.yml) only checks if both applications build correctly but doesn't run any tests (unit tests are not implemented).

Applications have been built and tested with QtCreator on Windows 10 just to make sure that they work correctly on this system.

In prebuilt_linux directory there are binaries that should work on most modern linux distributions. If not, try to build them by yourself.
