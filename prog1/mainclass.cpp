/* author: Pawel Rasinski */

#include "mainclass.h"

MainClass::MainClass(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(QGuiApplication::applicationDisplayName());

    server = new QLocalServer(this);
    server->listen(SERVER_NAME);
    clientIsConnected = false;
    connect(server, &QLocalServer::newConnection, this, &MainClass::newConnection);

    statusLabel = new QLabel;
    statusLabel->setText("");

    msgEdit = new QLineEdit;
    sendButton = new QPushButton("WYSLIJ");
    connect(sendButton, &QPushButton::clicked, this, &MainClass::sendMsg);

    playButton = new QPushButton("LOSUJ");
    connect(playButton, &QPushButton::clicked, this, &MainClass::drawNumbersCmd);

    quitButton = new QPushButton("ZAMKNIJ");
    connect(quitButton, &QPushButton::clicked, this, &MainClass::closeProgramCmd);

    mainLayout = new QGridLayout(this);
    mainLayout->addWidget(statusLabel, 0, 0, 1, 2);
    mainLayout->addWidget(msgEdit, 1, 0);
    mainLayout->addWidget(sendButton, 1, 1);
    mainLayout->addWidget(playButton, 2, 1);
    mainLayout->addWidget(quitButton, 3, 1);

    processProg2 = new QProcess(this);
    processProg2->start("prog2");
}

MainClass::~MainClass()
{
    delete mainLayout;
    delete statusLabel;
    delete msgEdit;
    delete sendButton;
    delete playButton;
    delete quitButton;
    delete processProg2;
    delete socketConnection;
    delete server;
}

void MainClass::newConnection()
{
    socketConnection = server->nextPendingConnection();
    clientIsConnected = true;
    connect(socketConnection, &QLocalSocket::readyRead, this, &MainClass::readMsg);
}

void MainClass::sendMsg()
{
    QString msg = msgEdit->text();

    if (msg.size() == 0)
        return;

    if (msg.toLower() == "losuj") {
        drawNumbersCmd();
        msgEdit->clear();
        return;
    }
    else if (msg.toLower() == "zamknij") {
        closeProgramCmd();
    }

    QByteArray msgByte = encodeMsg(QString("chat"), msg);

    if (clientIsConnected) {
        socketConnection->write(msgByte);
        socketConnection->flush();
    }

    msgEdit->clear();
}

void MainClass::readMsg()
{
    QByteArray msgByte = socketConnection->readAll();
    QJsonObject jsonObj = decodeMsg(msgByte);

    if (jsonObj["cmdType"] == "wygrana") {
        statusLabel->setText("WYGRANA: " + jsonObj["params"].toString());
    }

}

void MainClass::drawNumbersCmd()
{
    statusLabel->setText("BRAK WYGRANEJ");

    QByteArray msgByte = encodeMsg(QString("losuj"), QString("null"));

    if (clientIsConnected) {
        socketConnection->write(msgByte);
        socketConnection->flush();
    }
}

void MainClass::closeProgramCmd()
{
    QByteArray msgByte = encodeMsg(QString("zamknij"), QString("null"));

    if (clientIsConnected) {
        socketConnection->write(msgByte);
        socketConnection->flush();
    }

    processProg2->waitForFinished(1000);
    close();
}

QByteArray MainClass::encodeMsg(QString cmd, QString params)
{
    QJsonObject jsonObj;
    jsonObj["cmdType"] = cmd;
    jsonObj["params"] = params;
    QJsonDocument jsonDoc(jsonObj);
    QByteArray msgByte = jsonDoc.toJson();

    return msgByte;
}

QJsonObject MainClass::decodeMsg(QByteArray msgByte)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(msgByte);
    QJsonObject jsonObj = jsonDoc.object();

    return jsonObj;
}
