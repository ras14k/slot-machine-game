/* author: Pawel Rasinski */

#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QtWidgets>
#include <QtNetwork>
#include <QDialog>
#include <qlocalserver.h>
#include <qlocalsocket.h>

#define SERVER_NAME "slot_machine"

QT_BEGIN_NAMESPACE
class QLabel;
class QLineEdit;
class QPushButton;
class QLocalServer;
class QLocalSocket;
class QString;
QT_END_NAMESPACE

class MainClass : public QDialog
{
    Q_OBJECT

public:
    explicit MainClass(QWidget *parent = nullptr);
    ~MainClass();

private slots:
    void newConnection();
    void sendMsg();
    void readMsg();
    void drawNumbersCmd();
    void closeProgramCmd();

private:
    QByteArray encodeMsg(QString cmd, QString params);
    QJsonObject decodeMsg(QByteArray msgByte);
    bool clientIsConnected;
    QLocalServer *server;
    QLocalSocket *socketConnection;
    QGridLayout *mainLayout;
    QLabel *statusLabel;
    QLineEdit *msgEdit;
    QPushButton *sendButton;
    QPushButton *playButton;
    QPushButton *quitButton;
    QProcess *processProg2;
};

#endif
