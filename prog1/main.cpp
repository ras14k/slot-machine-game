/* author: Pawel Rasinski */

#include <QApplication>
#include "mainclass.h"

int main(int argc, char *argv[])
{
    qputenv("QT_STYLE_OVERRIDE", "");

    QApplication app(argc, argv);
    QGuiApplication::setApplicationDisplayName("Program 1");

    MainClass prog1;
    prog1.setWindowFlags ( Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    prog1.show();

    return app.exec();
}
