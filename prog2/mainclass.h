/* author: Pawel Rasinski */

#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QtWidgets>
#include <QtNetwork>
#include <QDialog>
#include <qlocalsocket.h>
#include <cstdlib>

#define SERVER_NAME "slot_machine"

QT_BEGIN_NAMESPACE
class QLabel;
class QLineEdit;
class QListWidget;
class QPushButton;
class QLocalSocket;
QT_END_NAMESPACE

class MainClass : public QDialog
{
    Q_OBJECT

public:
    explicit MainClass(QWidget *parent = nullptr);
    ~MainClass();

private slots:
    void readMsg();

private:
    void drawNumbers();
    QByteArray encodeMsg(QString cmd, QString params);
    QJsonObject decodeMsg(QByteArray msgByte);
    QLocalSocket *socket;
    QGridLayout *mainLayout;
    QLabel *numbersLabel;
    QListWidget *chatArea;
};

#endif
