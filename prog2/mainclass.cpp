/* author: Pawel Rasinski */

#include "mainclass.h"

MainClass::MainClass(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(QGuiApplication::applicationDisplayName());

    socket = new QLocalSocket(this);
    connect(socket, &QLocalSocket::readyRead, this, &MainClass::readMsg);

    numbersLabel = new QLabel(tr("| # | # | # |"));
    chatArea = new QListWidget();

    mainLayout = new QGridLayout(this);
    mainLayout->addWidget(numbersLabel, 0, 1);
    mainLayout->addWidget(chatArea, 1, 0, 2, 3);

    socket->connectToServer(SERVER_NAME);
}

MainClass::~MainClass()
{
    delete mainLayout;
    delete numbersLabel;
    delete chatArea;
    delete socket;
}

void MainClass::readMsg()
{
    QByteArray msgByte = socket->readAll();
    QJsonObject jsonObj = decodeMsg(msgByte);

    if (jsonObj["cmdType"] == "chat") {
        chatArea->addItem(jsonObj["params"].toString());
        chatArea->scrollToBottom();
        return;
    }

    if (jsonObj["cmdType"] == "losuj") {
        drawNumbers();
        return;
    }

    if (jsonObj["cmdType"] == "zamknij") {
        close();
    }
}

void MainClass::drawNumbers()
{
    int x = std::rand() % 3 + 1;
    int y = std::rand() % 3 + 1;
    int z = std::rand() % 3 + 1;

    QString randomNumbers = QString("| %1 | %2 | %3 |").arg(QString::number(x),
                                                            QString::number(y),
                                                            QString::number(z));
    numbersLabel->setText(randomNumbers);

    QByteArray msgByte = encodeMsg(QString("wygrana"), randomNumbers);

    if ((x == y) && (y == z)) {
        socket->write(msgByte);
        socket->flush();
    }
}

QByteArray MainClass::encodeMsg(QString cmd, QString params)
{
    QJsonObject jsonObj;
    jsonObj["cmdType"] = cmd;
    jsonObj["params"] = params;
    QJsonDocument jsonDoc(jsonObj);
    QByteArray msgByte = jsonDoc.toJson();

    return msgByte;
}

QJsonObject MainClass::decodeMsg(QByteArray msgByte)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(msgByte);
    QJsonObject jsonObj = jsonDoc.object();

    return jsonObj;
}
