/* author: Pawel Rasinski */

#include <QApplication>
#include "mainclass.h"

int main(int argc, char *argv[])
{
    qputenv("QT_STYLE_OVERRIDE", "");

    QApplication app(argc, argv);
    QGuiApplication::setApplicationDisplayName("Program 2");

    MainClass prog2;
    prog2.setWindowFlags( Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    prog2.show();

    return app.exec();
}
